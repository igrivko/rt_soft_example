#!/usr/bin/env python
 
from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import cgi
import re
import shutil
from io import BytesIO
from openpyxl import load_workbook

def modelToHtml(excel_file):
    wb = load_workbook(excel_file)
    sheet_name = wb.get_sheet_names()[0]
    ws = wb.get_sheet_by_name(sheet_name)
    items = []
    for i in range(3, ws.max_row+1):
        if ws.cell(row=i, column=1).value == None:
            break
        items.append(ws.cell(row=i, column=1).value)
    connections = []
    for i in range(3, ws.max_row+1):
        if ws.cell(row=i, column=3).value == None or ws.cell(row=i, column=4).value == None:
            break
        i1 = ws.cell(row=i, column=3).value
        i2 = ws.cell(row=i, column=4).value
        if i1 in items and i2 in items:
            connections.append([items.index(i1), items.index(i2)])

    print("Excel file", excel_file)
    print("max row", ws.max_row)
    print("max_column", ws.max_column)
    base, ext = os.path.splitext(excel_file)
    html_file = base.replace('/models', '/html_models') + ".html"
    f = open(html_file, 'w')
    s1 = """
<!doctype html>
<html lang="en">

<head>
<meta charset="UTF-8">

<style type="text/css">
    h4 {font-family: sans-serif;}
    p {font-family: sans-serif;}
    a {font-family: sans-serif; color:#d15423; text-decoration:none;}
</style>
    
<title>RT Soft Example</title>
<script type="text/javascript">


window.addEventListener("load", canvasApp, false);


var mouseDown = false;
var scale = 1.0;
var scaleMultiplier = 0.8;
var startDragOffset = {};
var translatePos = {x: 0, y: 0};




function canvasApp() 
{
      
      var theCanvas = document.getElementById("canvasOne");
      var context = theCanvas.getContext("2d");

      // add button event listeners
      document.getElementById("plus").addEventListener("click", function(){
      scale /= scaleMultiplier;
      drawScreen();
      }, false);

      document.getElementById("minus").addEventListener("click", function(){
      scale *= scaleMultiplier;
      drawScreen();
      }, false);


      // add event listeners to handle screen drag
      theCanvas.addEventListener("mousedown", function(evt){
      mouseDown = true;
      startDragOffset.x = evt.clientX - translatePos.x;
      startDragOffset.y = evt.clientY - translatePos.y;
      });

      theCanvas.addEventListener("mouseup", function(evt){
      mouseDown = false;
      });

      theCanvas.addEventListener("mouseover", function(evt){
      mouseDown = false;
      });

      theCanvas.addEventListener("mouseout", function(evt){
      mouseDown = false;
      });

      theCanvas.addEventListener("mousemove", function(evt){
      if (mouseDown) {
      translatePos.x = evt.clientX - startDragOffset.x;
      translatePos.y = evt.clientY - startDragOffset.y;
      drawScreen();
      }
      });

"""
   
    s2 = "var shape_numbers = "
    s2 += str(items) + ";"
    s2 += "var connections = "
    s2 += str(connections) + ";"

    s3 = """
      var shapes;
  
      init();
      
      
  function init() 
  {
      shapes = [];
      makeShapes();
      drawScreen();    
  }
      
  function makeShapes() 
  {
      var i;
      var tempX;
      var tempY;
      var x_beg = 12;
      var y_beg = 12;
      var x_pos = x_beg;
      var y_pos = y_beg;
      for (i=0; i < shape_numbers.length; i++)
      {
        var num = shape_numbers[i];
        tempX = x_pos;
        tempY = y_pos;
        tempShape = {x:tempX, y:tempY, n:num};
        shapes.push(tempShape);
        if(x_pos + 45 > theCanvas.width - x_beg)
        {
           x_pos = x_beg;
           y_pos += 45;
        }
        else
        {
           x_pos += 45;
        }
      }
  }

  function drawConnections() 
  {
    var i;
    for (i=0; i < connections.length; i++)
    {
       var i1 = connections[i][0];
       var i2 = connections[i][1];
       //Line	
       context.beginPath();
       context.moveTo(shapes[i1].x, shapes[i1].y);
       context.lineTo(shapes[i2].x, shapes[i2].y);
       context.lineWidth = 0;
       context.strokeStyle = '#000000';
       context.stroke();
    }
  }

	            
  function drawShapes() 
  {
    var i;
    for (i=0; i < shapes.length; i++)
    {
      context.fillStyle = "#ffa500";
      context.beginPath();
      context.arc(shapes[i].x, shapes[i].y, 12, 0, 2*Math.PI, false);
      context.closePath();
      context.fill();
      context.fillStyle = "black";
      context.font="8px Verdana";
      var label = shapes[i].n.toString();
      var offset = -5;
      if(label.length == 3)
           offset = -7;
      else if(label.length == 4)
           offset = -10;
      else if(label.length == 1)
           offset = -3;
      context.fillText(shapes[i].n.toString(), shapes[i].x+offset, shapes[i].y+3);
    }
  }
                                  
  function drawScreen() 
  {
    //bg
    context.fillStyle = "#f0f0f0";
    context.fillRect(0,0,theCanvas.width,theCanvas.height);
    context.save();
    context.translate(translatePos.x, translatePos.y);
    context.scale(scale, scale);

    drawConnections();
    drawShapes();
    context.restore();
  }
} //function canvasApp()

</script>

</head>
<body>
  <div style="top: 50px; text-align:center">
  <h4>Model</h4>
  <canvas id="canvasOne" width="1000" height="700">
  Your browser does not support HTML5 canvas.
  </canvas>

      <div id="buttonWrapper">
      <input type="button" id="plus" value="+"><input type="button" id="minus" value="-">
      </div>


    </div>
</body>
</html>

"""
    f.write(s1)
    f.write(s2)
    f.write(s3)
    f.close()


 
# HTTPRequestHandler class
class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
 
    # GET
    def do_GET(self):
        print(self.path)
        # Send response status code
        self.send_response(200)
 
        # Send headers
        self.send_header('Content-type','text/html')
        self.end_headers()
 
        # Prepend the file name with current dir path.
        if self.path == "/":
                path = os.getcwd() + "/start_page.html"
        elif self.path == "/view_loaded_models.html":
                self.wfile.write(self.getLoadedModelsHtml().encode())
                return
        else:
                path = os.getcwd() + self.path
                
 
        # Check if file exists there.
        if os.path.isfile(path):
 
            # Read the file and return its contents.
            with open(path) as fileHandle:
                # Send file content.
                self.wfile.write(fileHandle.read().encode())
 
        # Fail with 404 File Not Found error.
        else:
            # Send headers with error.
            self.send_header('Content-type', 'text/html')
            self.send_response(404, 'File Not Found')
            self.end_headers()

    def getLoadedModelsHtml(self):
        str = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">'
        str += "<html>\n<title>Loaded Models</title>\n"
        str += "<body>\n<h2>Loaded Models</h2>\n"
        str += "<ul>\n"
        lst = os.listdir(os.getcwd() + "/html_models")
        for item in lst:
            str += '<li><a href="html_models/%s"/>%s</li>\n' % (item, item)
        str += "</ul>\n"
        str += "</body></html>\n"
        return str

    def do_POST(self):
        """Serve a POST request."""
        r, info = self.deal_post_data()
        print((r, info, "by: ", self.client_address))
        f = BytesIO()
        f.write(b'<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">')
        f.write(b"<html>\n<title>Upload Result Page</title>\n")
        f.write(b"<body>\n<h2>Upload Result Page</h2>\n")
        f.write(b"<hr>\n")
        if r:
            f.write(b"<strong>Success:</strong>")
        else:
            f.write(b"<strong>Failed:</strong>")
        f.write(info.encode())
        f.write(("<br><a href=\"%s\">Back to Load Page</a>" % self.headers['referer']).encode())
        f.write(("<br><a href=\"view_loaded_models.html\">View Models</a>").encode())
        f.write(b"<hr>")
        f.write(b"</body>\n</html>\n")
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            self.copyfile(f, self.wfile)
            f.close()

    def copyfile(self, source, outputfile):
        """Copy all data between two file objects.
        The SOURCE argument is a file object open for reading
        (or anything with a read() method) and the DESTINATION
        argument is a file object open for writing (or
        anything with a write() method).
        The only reason for overriding this would be to change
        the block size or perhaps to replace newlines by CRLF
        -- note however that this the default server uses this
        to copy binary data as well.
        """
        shutil.copyfileobj(source, outputfile)

        
    def deal_post_data(self):
        content_type = self.headers['content-type']
        if not content_type:
            return (False, "Content-Type header doesn't contain boundary")
        boundary = content_type.split("=")[1].encode()
        remainbytes = int(self.headers['content-length'])
        line = self.rfile.readline()
        remainbytes -= len(line)
        if not boundary in line:
            return (False, "Content NOT begin with boundary")
        line = self.rfile.readline()
        remainbytes -= len(line)
        fn = re.findall(r'Content-Disposition.*name="file"; filename="(.*)"', line.decode())
        if not fn:
            return (False, "Can't find out file name...")
        path = os.getcwd() + self.path
        fn = os.path.join(path, fn[0])
        line = self.rfile.readline()
        remainbytes -= len(line)
        line = self.rfile.readline()
        remainbytes -= len(line)
        try:
            out = open(fn, 'wb')
        except IOError:
            return (False, "Can't create file to write, do you have permission to write?")
                
        preline = self.rfile.readline()
        remainbytes -= len(preline)
        while remainbytes > 0:
            line = self.rfile.readline()
            remainbytes -= len(line)
            if boundary in line:
                preline = preline[0:-1]
                if preline.endswith(b'\r'):
                    preline = preline[0:-1]
                out.write(preline)
                out.close()
                filename, file_extension = os.path.splitext(fn)
                print("file_extension",  file_extension)
                if file_extension == ".xlsx" or file_extension == ".xls":
                    modelToHtml(fn)
                return (True, "File '%s' upload success!" % fn)
            else:
                out.write(preline)
                preline = line
        return (False, "Unexpect Ends of data.")
 
def run():
  print('starting server...')
 
  # Server settings
  # Choose port 2016
  server_address = ('127.0.0.1', 2016)
  httpd = HTTPServer(server_address, testHTTPServer_RequestHandler)
  print('running server on localhost:2016 ...')
  httpd.serve_forever()
 
 
run()
